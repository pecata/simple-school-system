# Simple School System

### Task Requirements
We need an application for storing Students and Teachers information like name, age, group and courses. We have two type of courses -Main and Secondary.

The application should be able to add remove or modify students and teachers. With this application we should be able to create different reports:
- How many students we have;
- How many teachers we have;
- How many courses by type we have;
- Which students participate in specific course;
- Which students participate in specific group;
- Find all teachers and students for specific group and course;
- Find all students older than specific age and participate in specific course.

The application should provide public API.
