CREATE TABLE people
(
    id          SERIAL PRIMARY KEY NOT NULL,
    first_name  VARCHAR(64)        NOT NULL,
    middle_name VARCHAR(64)        NOT NULL,
    last_name   VARCHAR(64)        NOT NULL,
    tax_id      CHAR(10)           NOT NULL UNIQUE,
    birthday    DATE               NOT NULL
);

CREATE TABLE students
(
    id           SERIAL PRIMARY KEY NOT NULL,
    person_id    INTEGER            NOT NULL REFERENCES people (id),
    group_number smallint           NOT NULL
);

CREATE TABLE teachers
(
    id           SERIAL PRIMARY KEY NOT NULL,
    person_id    INTEGER            NOT NULL REFERENCES people (id),
    group_number smallint           NOT NULL
);

CREATE TYPE course_type AS ENUM ('MAIN', 'SECONDARY');

CREATE TABLE courses
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name VARCHAR(64)        NOT NULL UNIQUE,
    type course_type        NOT NULL
);

CREATE TYPE courses_people_type AS ENUM ('STUDENT', 'TEACHER');

CREATE TABLE courses_people
(
    id        SERIAL PRIMARY KEY  NOT NULL,
    course_id INTEGER             NOT NULL REFERENCES courses (id),
    person_id INTEGER             NOT NULL REFERENCES people (id),
    type      courses_people_type NOT NULL,
    UNIQUE (course_id, person_id)
);
