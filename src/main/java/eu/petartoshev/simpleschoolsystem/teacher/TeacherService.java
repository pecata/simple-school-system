package eu.petartoshev.simpleschoolsystem.teacher;

import eu.petartoshev.simpleschoolsystem.people.PersonService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TeacherService
{

	private final PersonService personService;
	private final TeacherRepository teacherRepository;

	public TeacherService(final PersonService personService,
						  final TeacherRepository teacherRepository)
	{
		this.personService = personService;
		this.teacherRepository = teacherRepository;
	}

	private static Teacher createFromRequest(final Integer teacherId,
											 final Integer personId,
											 final TeacherRequest teacherRequest)
	{
		return new Teacher(teacherId,
						   personId,
						   teacherRequest.getGroupNumber());
	}

	public Integer persist(final TeacherRequest teacherRequest)
	{
		final Integer personId = personService.persist(teacherRequest.getPersonRequest());
		final Teacher teacher = createFromRequest(null, personId, teacherRequest);
		return teacherRepository.persist(teacher);
	}

	public void update(final Integer teacherId,
					   final TeacherRequest teacherRequest)
	{
		final Teacher teacher = getById(teacherId);
		personService.update(teacher.personId(), teacherRequest.getPersonRequest());
		final Teacher updatedTeacher = createFromRequest(teacher.id(), teacher.personId(), teacherRequest);
		teacherRepository.update(updatedTeacher);
	}

	public Teacher getById(final Integer id)
	{
		final List<Teacher> people = teacherRepository.get(List.of(id));
		if (people.isEmpty())
		{
			return null;
		}
		return people.get(0);
	}

	public List<Teacher> getByIds(final Collection<Integer> ids)
	{
		if (ids.isEmpty())
		{
			return List.of();
		}
		return teacherRepository.get(ids);
	}

	public List<Teacher> getAll()
	{
		return teacherRepository.getAll();
	}

	public Teacher getByPersonId(final Integer personId)
	{
		return teacherRepository.getByPersonId(personId);
	}
}
