package eu.petartoshev.simpleschoolsystem.teacher;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.petartoshev.simpleschoolsystem.people.PersonRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TeacherRequest
{

	private PersonRequest personRequest;
	private Short groupNumber;

	public TeacherRequest()
	{
	}

	public TeacherRequest(final PersonRequest personRequest,
						  final Short groupNumber)
	{
		this.personRequest = personRequest;
		this.groupNumber = groupNumber;
	}

	public PersonRequest getPersonRequest()
	{
		return personRequest;
	}

	public Short getGroupNumber()
	{
		return groupNumber;
	}
}
