package eu.petartoshev.simpleschoolsystem.teacher;

public record Teacher(Integer id,
					  Integer personId,
					  Short groupNumber)
{

}
