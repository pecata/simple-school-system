package eu.petartoshev.simpleschoolsystem.teacher;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
class TeacherRepository
		extends NamedParameterJdbcDaoSupport
{

	private static final TeacherRowMapper TEACHER_ROW_MAPPER = new TeacherRowMapper();

	public TeacherRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	private static SqlParameterSource toParameters(final Teacher teacher)
	{
		return new MapSqlParameterSource()
				.addValue("id", teacher.id())
				.addValue("personId", teacher.personId())
				.addValue("groupNumber", teacher.groupNumber());
	}

	public Integer persist(final Teacher teacher)
	{
		final String sql = """
				INSERT INTO teachers (person_id,
				                      group_number)
				            VALUES(:personId,
				                   :groupNumber)
				""";
		final SqlParameterSource params = toParameters(teacher);

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, params, keyHolder, new String[]{"id"});
		return (Integer) keyHolder.getKey();
	}

	public void update(final Teacher teacher)
	{
		final String sql = """
				UPDATE teachers 
				   SET person_id = :personId,
				       group_number = :groupNumber
				 WHERE id = :id
				""";
		final SqlParameterSource params = toParameters(teacher);

		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public List<Teacher> get(final Collection<Integer> ids)
	{
		final String sql = "SELECT * FROM teachers WHERE id IN (:ids)";
		final Map<String, Object> params = Map.of("ids", ids);
		return getNamedParameterJdbcTemplate().query(sql, params, TEACHER_ROW_MAPPER);
	}

	public List<Teacher> getAll()
	{
		final String sql = "SELECT * FROM teachers";
		return getNamedParameterJdbcTemplate().query(sql, TEACHER_ROW_MAPPER);
	}

	public Teacher getByPersonId(final Integer personId)
	{
		final String sql = "SELECT * FROM teachers WHERE person_id = :personId";
		final Map<String, Object> params = Map.of("personId", personId);
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, params, TEACHER_ROW_MAPPER);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private static class TeacherRowMapper
			implements RowMapper<Teacher>
	{

		@Override
		public Teacher mapRow(final ResultSet rs,
							  final int rowNum)
				throws SQLException
		{
			final Integer id = rs.getInt("id");
			final Integer personId = rs.getInt("person_id");
			final Short groupNumber = rs.getShort("group_number");

			return new Teacher(id,
							   personId,
							   groupNumber);
		}
	}
}
