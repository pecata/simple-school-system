package eu.petartoshev.simpleschoolsystem.teacher;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/teachers")
public class TeacherController
{

	private final TeacherService teacherService;

	public TeacherController(final TeacherService teacherService)
	{
		this.teacherService = teacherService;
	}

	@PostMapping
	public Integer persist(@RequestBody final TeacherRequest teacherRequest)
	{
		return teacherService.persist(teacherRequest);
	}

	@PutMapping("/{teacherId}")
	public void update(@PathVariable final Integer teacherId,
					   @RequestBody final TeacherRequest teacherRequest)
	{
		teacherService.update(teacherId, teacherRequest);
	}

	@GetMapping("/{teacherId}")
	public Teacher getById(@PathVariable final Integer teacherId)
	{
		return teacherService.getById(teacherId);
	}

	@GetMapping
	public List<Teacher> getAll()
	{
		return teacherService.getAll();
	}
}
