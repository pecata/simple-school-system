package eu.petartoshev.simpleschoolsystem.course;

public class CourseValidationService
{

	public static void validate(final CourseRequest courseRequest)
	{
		validateString(courseRequest.getName(), "name", 64);
	}

	private static void validateString(final String s,
									   final String key,
									   final Integer maxLength)
	{
		if (s == null)
		{
			throw new CourseDataException(key + " is null");
		}
		else if (s.isBlank())
		{
			throw new CourseDataException(key + " is blank");
		}
		else if (maxLength != null && s.length() > maxLength)
		{
			throw new CourseDataException(key + " is too long");
		}
	}
}
