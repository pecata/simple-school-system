package eu.petartoshev.simpleschoolsystem.course;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class CourseService
{

	private final CourseRepository courseRepository;

	public CourseService(final CourseRepository courseRepository)
	{
		this.courseRepository = courseRepository;
	}

	private static Course createFromRequest(final Integer courseId,
											final CourseRequest courseRequest)
	{
		CourseValidationService.validate(courseRequest);
		return new Course(courseId,
						  courseRequest.getName(),
						  courseRequest.getType());
	}

	public Integer persist(final CourseRequest courseRequest)
	{
		final Course course = createFromRequest(null, courseRequest);
		return courseRepository.persist(course);
	}

	public void update(final Integer courseId,
					   final CourseRequest courseRequest)
	{
		final Course course = createFromRequest(courseId, courseRequest);
		courseRepository.update(course);
	}

	public Course getById(final Integer id)
	{
		final List<Course> people = courseRepository.get(List.of(id));
		if (people.isEmpty())
		{
			return null;
		}
		return people.get(0);
	}

	public List<Course> getByIds(final Collection<Integer> ids)
	{
		return courseRepository.get(ids);
	}

	public Course getByName(final String name)
	{
		return courseRepository.getByName(name);
	}

	public List<Course> getByType(final CourseType type)
	{
		return courseRepository.getByType(type);
	}

	public List<Course> getAll()
	{
		return courseRepository.getAll();
	}
}
