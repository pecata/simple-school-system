package eu.petartoshev.simpleschoolsystem.course;

public record Course(Integer id,
					 String name,
					 CourseType type)
{

}
