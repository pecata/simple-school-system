package eu.petartoshev.simpleschoolsystem.course;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/courses")
public class CourseController
{

	private final CourseService courseService;

	public CourseController(final CourseService courseService)
	{
		this.courseService = courseService;
	}

	@PostMapping
	public void persist(@RequestBody final CourseRequest courseRequest)
	{
		courseService.persist(courseRequest);
	}

	@PutMapping("/{courseId}")
	public void update(@PathVariable final Integer courseId,
					   @RequestBody final CourseRequest courseRequest)
	{
		courseService.update(courseId, courseRequest);
	}

	@GetMapping("/{courseId}")
	public Course getById(@PathVariable final Integer courseId)
	{
		return courseService.getById(courseId);
	}

	@GetMapping("/by-name/{name}")
	public Course getByName(@PathVariable final String name)
	{
		return courseService.getByName(name);
	}

	@GetMapping("/by-type/{type}")
	public List<Course> getByType(@PathVariable final CourseType type)
	{
		return courseService.getByType(type);
	}

	@GetMapping
	public List<Course> getAll()
	{
		return courseService.getAll();
	}
}
