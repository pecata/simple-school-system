package eu.petartoshev.simpleschoolsystem.course;

public enum CourseType
{
	MAIN,
	SECONDARY
}
