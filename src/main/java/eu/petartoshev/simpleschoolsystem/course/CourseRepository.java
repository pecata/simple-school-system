package eu.petartoshev.simpleschoolsystem.course;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
class CourseRepository
		extends NamedParameterJdbcDaoSupport
{

	private static final CourseRowMapper COURSE_ROW_MAPPER = new CourseRowMapper();

	public CourseRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	private static SqlParameterSource toParameters(final Course course)
	{
		return new MapSqlParameterSource()
				.addValue("id", course.id())
				.addValue("name", course.name())
				.addValue("type", course.type().name(), Types.OTHER);
	}

	public Integer persist(final Course course)
	{
		final String sql = """
				INSERT INTO courses (name,
				                     type)
				            VALUES(:name,
				                   :type)
				""";
		final SqlParameterSource params = toParameters(course);

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, params, keyHolder, new String[]{"id"});
		return (Integer) keyHolder.getKey();
	}

	public void update(final Course course)
	{
		final String sql = """
				UPDATE courses 
				   SET name = :name,
				       type = :type
				 WHERE id = :id
				""";
		final SqlParameterSource params = toParameters(course);

		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public List<Course> get(final Collection<Integer> ids)
	{
		final String sql = "SELECT * FROM courses WHERE id IN (:ids)";
		final Map<String, Object> params = Map.of("ids", ids);
		return getNamedParameterJdbcTemplate().query(sql, params, COURSE_ROW_MAPPER);
	}

	public Course getByName(final String name)
	{
		final String sql = "SELECT * FROM courses WHERE name = :name";
		final Map<String, Object> params = Map.of("name", name);
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, params, COURSE_ROW_MAPPER);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	public List<Course> getByType(final CourseType type)
	{
		final String sql = "SELECT * FROM courses WHERE type = :type";
		final Map<String, Object> params = Map.of("type", type.name());
		return getNamedParameterJdbcTemplate().query(sql, params, COURSE_ROW_MAPPER);
	}

	public List<Course> getAll()
	{
		final String sql = "SELECT * FROM courses";
		return getNamedParameterJdbcTemplate().query(sql, COURSE_ROW_MAPPER);
	}

	private static class CourseRowMapper
			implements RowMapper<Course>
	{

		@Override
		public Course mapRow(final ResultSet rs,
							 final int rowNum)
				throws SQLException
		{
			final Integer id = rs.getInt("id");
			final String name = rs.getString("name");
			final CourseType type = CourseType.valueOf(rs.getString("type"));

			return new Course(id,
							  name,
							  type);
		}
	}
}
