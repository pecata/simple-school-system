package eu.petartoshev.simpleschoolsystem.course;

public class CourseDataException
	extends RuntimeException
{

	public CourseDataException(final String message)
	{
		super(message);
	}
}
