package eu.petartoshev.simpleschoolsystem.course;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CourseRequest
{
	private String name;
	private CourseType type;

	public CourseRequest()
	{
	}

	public CourseRequest(final String name,
						 final CourseType type)
	{
		this.name = name;
		this.type = type;
	}

	public String getName()
	{
		return name;
	}

	public CourseType getType()
	{
		return type;
	}
}
