package eu.petartoshev.simpleschoolsystem.people;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

public class PersonRequest
{

	private String firstName;
	private String middleName;
	private String lastName;
	private String taxId;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	private LocalDate birthday;

	public PersonRequest()
	{
	}

	public PersonRequest(final String firstName,
						 final String middleName,
						 final String lastName,
						 final String taxId,
						 final LocalDate birthday)
	{
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.taxId = taxId;
		this.birthday = birthday;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public String getMiddleName()
	{
		return middleName;
	}

	public String getLastName()
	{
		return lastName;
	}

	public String getTaxId()
	{
		return taxId;
	}

	public LocalDate getBirthday()
	{
		return birthday;
	}
}
