package eu.petartoshev.simpleschoolsystem.people;

import java.time.LocalDate;

public record Person(Integer id,
					 String firstName,
					 String middleName,
					 String lastName,
					 String taxId,
					 LocalDate birthday)
{

}
