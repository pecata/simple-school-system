package eu.petartoshev.simpleschoolsystem.people;

public class PersonValidationService
{

	public static void validate(final PersonRequest personRequest)
	{
		validateString(personRequest.getFirstName(), "firstName", 64);
		validateString(personRequest.getMiddleName(), "middleName", 64);
		validateString(personRequest.getLastName(), "lastName", 64);
		validateTaxId(personRequest.getTaxId());
	}

	private static void validateString(final String s,
									   final String key,
									   final Integer maxLength)
	{
		if (s == null)
		{
			throw new PersonDataException(key + " is null");
		}
		else if (s.isBlank())
		{
			throw new PersonDataException(key + " is blank");
		}
		else if (maxLength != null && s.length() > maxLength)
		{
			throw new PersonDataException(key + " is too long");
		}
	}

	private static void validateTaxId(final String taxId)
	{
		final int length = 10;
		validateString(taxId, "taxId", length);
		if (taxId.trim().length() != length)
		{
			throw new PersonDataException("taxId does not match with length");
		}
	}
}
