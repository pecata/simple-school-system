package eu.petartoshev.simpleschoolsystem.people;

public class PersonDataException
		extends RuntimeException
{

	public PersonDataException(final String message)
	{
		super(message);
	}
}
