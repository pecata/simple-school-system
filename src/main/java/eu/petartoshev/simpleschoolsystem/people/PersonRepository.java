package eu.petartoshev.simpleschoolsystem.people;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
class PersonRepository
		extends NamedParameterJdbcDaoSupport
{

	private static final PersonRowMapper PERSON_ROW_MAPPER = new PersonRowMapper();

	public PersonRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	private static SqlParameterSource toParameters(final Person person)
	{
		return new MapSqlParameterSource()
				.addValue("id", person.id())
				.addValue("firstName", person.firstName())
				.addValue("middleName", person.middleName())
				.addValue("lastName", person.lastName())
				.addValue("taxId", person.taxId())
				.addValue("birthday", person.birthday());
	}

	public Integer persist(final Person person)
	{
		final String sql = """
				INSERT INTO people (first_name,
				                    middle_name,
				                    last_name,
				                    tax_id,
				                    birthday)
				            VALUES(:firstName,
				                   :middleName,
				                   :lastName,
				                   :taxId,
				                   :birthday)
				""";
		final SqlParameterSource params = toParameters(person);

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, params, keyHolder, new String[]{"id"});
		return (Integer) keyHolder.getKey();
	}

	public void update(final Person person)
	{
		final String sql = """
				UPDATE people 
				   SET first_name = :firstName,
				       middle_name = :middleName,
				       last_name = :lastName,
				       tax_id = :taxId,
				       birthday = :birthday
				 WHERE id = :id
				""";
		final SqlParameterSource params = toParameters(person);

		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public List<Person> get(final Collection<Integer> ids)
	{
		final String sql = "SELECT * FROM people WHERE id IN (:ids)";
		final Map<String, Object> params = Map.of("ids", ids);
		return getNamedParameterJdbcTemplate().query(sql, params, PERSON_ROW_MAPPER);
	}

	public List<Person> getAll()
	{
		final String sql = "SELECT * FROM people";
		return getNamedParameterJdbcTemplate().query(sql, PERSON_ROW_MAPPER);
	}

	public Person getByTaxId(final String taxId)
	{
		final String sql = "SELECT * FROM people WHERE tax_id  = :taxId";
		final Map<String, Object> params = Map.of("taxId", taxId);
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, params, PERSON_ROW_MAPPER);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private static class PersonRowMapper
			implements RowMapper<Person>
	{

		@Override
		public Person mapRow(final ResultSet rs,
							 final int rowNum)
				throws SQLException
		{
			final Integer id = rs.getInt("id");
			final String firstName = rs.getString("first_name");
			final String middleName = rs.getString("middle_name");
			final String lastName = rs.getString("last_name");
			final String taxId = rs.getString("tax_id");
			final LocalDate birthday = rs.getObject("birthday", LocalDate.class);

			return new Person(id,
							  firstName,
							  middleName,
							  lastName,
							  taxId,
							  birthday);
		}
	}
}
