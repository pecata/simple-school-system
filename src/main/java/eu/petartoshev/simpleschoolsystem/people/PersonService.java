package eu.petartoshev.simpleschoolsystem.people;

import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class PersonService
{

	private final PersonRepository personRepository;

	public PersonService(final PersonRepository personRepository)
	{
		this.personRepository = personRepository;
	}

	private static Person createFromRequest(final Integer personId,
											final PersonRequest personRequest)
	{
		PersonValidationService.validate(personRequest);
		return new Person(personId,
						  personRequest.getFirstName(),
						  personRequest.getMiddleName(),
						  personRequest.getLastName(),
						  personRequest.getTaxId(),
						  personRequest.getBirthday());
	}

	public Integer persist(final PersonRequest personRequest)
	{
		final Person person = createFromRequest(null, personRequest);
		return personRepository.persist(person);
	}

	public void update(final Integer personId,
					   final PersonRequest personRequest)
	{
		final Person person = createFromRequest(personId, personRequest);
		personRepository.update(person);
	}

	public Person getById(final Integer id)
	{
		final List<Person> people = personRepository.get(List.of(id));
		if (people.isEmpty())
		{
			return null;
		}
		return people.get(0);
	}

	public List<Person> getByIds(final Collection<Integer> ids)
	{
		if (ids.isEmpty())
		{
			return List.of();
		}
		return personRepository.get(ids);
	}

	public List<Person> getAll()
	{
		return personRepository.getAll();
	}

	public Person getByTaxId(final String taxId) {
		return personRepository.getByTaxId(taxId);
	}
}
