package eu.petartoshev.simpleschoolsystem.people;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/people")
public class PersonController
{

	private final PersonService personService;

	public PersonController(final PersonService personService)
	{
		this.personService = personService;
	}

	@GetMapping
	public List<Person> getAll() {
		return personService.getAll();
	}

	@GetMapping("/{personId}")
	public Person get(@PathVariable final Integer personId) {
		return personService.getById(personId);
	}
}
