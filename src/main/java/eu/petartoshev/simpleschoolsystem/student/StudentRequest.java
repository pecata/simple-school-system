package eu.petartoshev.simpleschoolsystem.student;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import eu.petartoshev.simpleschoolsystem.people.PersonRequest;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StudentRequest
{

	private PersonRequest personRequest;
	private Short groupNumber;

	public StudentRequest()
	{
	}

	public StudentRequest(final PersonRequest personRequest,
						  final Short groupNumber)
	{
		this.personRequest = personRequest;
		this.groupNumber = groupNumber;
	}

	public PersonRequest getPersonRequest()
	{
		return personRequest;
	}

	public Short getGroupNumber()
	{
		return groupNumber;
	}
}
