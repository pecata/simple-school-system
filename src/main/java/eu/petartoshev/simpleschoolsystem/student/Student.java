package eu.petartoshev.simpleschoolsystem.student;

public record Student(Integer id,
					  Integer personId,
					  Short groupNumber)
{

}
