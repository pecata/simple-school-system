package eu.petartoshev.simpleschoolsystem.student;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository
class StudentRepository
		extends NamedParameterJdbcDaoSupport
{

	private static final StudentRowMapper STUDENT_ROW_MAPPER = new StudentRowMapper();

	public StudentRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	private static SqlParameterSource toParameters(final Student student)
	{
		return new MapSqlParameterSource()
				.addValue("id", student.id())
				.addValue("personId", student.personId())
				.addValue("groupNumber", student.groupNumber());
	}

	public Integer persist(final Student student)
	{
		final String sql = """
				INSERT INTO students (person_id,
				                      group_number)
				            VALUES(:personId,
				                   :groupNumber)
				""";
		final SqlParameterSource params = toParameters(student);

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, params, keyHolder, new String[]{"id"});
		return (Integer) keyHolder.getKey();
	}

	public void update(final Student student)
	{
		final String sql = """
				UPDATE students 
				   SET person_id = :personId,
				       group_number = :groupNumber
				 WHERE id = :id
				""";
		final SqlParameterSource params = toParameters(student);

		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public List<Student> getByGroup(final Integer groupNumber)
	{
		final String sql = "SELECT * FROM students WHERE group_number = :groupNumber";
		final Map<String, Object> params = Map.of("groupNumber", groupNumber);
		return getNamedParameterJdbcTemplate().query(sql, params, STUDENT_ROW_MAPPER);
	}

	public List<Student> get(final Collection<Integer> ids)
	{
		final String sql = "SELECT * FROM students WHERE id IN (:ids)";
		final Map<String, Object> params = Map.of("ids", ids);
		return getNamedParameterJdbcTemplate().query(sql, params, STUDENT_ROW_MAPPER);
	}

	public List<Student> getAll()
	{
		final String sql = "SELECT * FROM students";
		return getNamedParameterJdbcTemplate().query(sql, STUDENT_ROW_MAPPER);
	}

	public Student getByPersonId(final Integer personId)
	{
		final String sql = "SELECT * FROM students WHERE person_id = :personId";
		final Map<String, Object> params = Map.of("personId", personId);
		try
		{
			return getNamedParameterJdbcTemplate().queryForObject(sql, params, STUDENT_ROW_MAPPER);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private static class StudentRowMapper
			implements RowMapper<Student>
	{

		@Override
		public Student mapRow(final ResultSet rs,
							  final int rowNum)
				throws SQLException
		{
			final Integer id = rs.getInt("id");
			final Integer personId = rs.getInt("person_id");
			final Short groupNumber = rs.getShort("group_number");

			return new Student(id,
							   personId,
							   groupNumber);
		}
	}
}
