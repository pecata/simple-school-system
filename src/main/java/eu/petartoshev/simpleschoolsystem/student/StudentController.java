package eu.petartoshev.simpleschoolsystem.student;

import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController
{

	private final StudentService studentService;

	public StudentController(final StudentService studentService)
	{
		this.studentService = studentService;
	}

	@PostMapping
	public Integer persist(@RequestBody final StudentRequest studentRequest)
	{
		return studentService.persist(studentRequest);
	}

	@PutMapping("/{studentId}")
	public void update(@PathVariable final Integer studentId,
					   @RequestBody final StudentRequest studentRequest)
	{
		studentService.update(studentId, studentRequest);
	}

	@GetMapping("/{studentId}")
	public Student getById(@PathVariable final Integer studentId)
	{
		return studentService.getById(studentId);
	}

	@GetMapping
	public List<Student> getAll()
	{
		return studentService.getAll();
	}

	@GetMapping("/groups/{groupNumber}")
	public List<Student> getByGroup(@PathVariable final Integer groupNumber)
	{
		return studentService.getByGroup(groupNumber);
	}
}
