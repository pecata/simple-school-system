package eu.petartoshev.simpleschoolsystem.student;

import eu.petartoshev.simpleschoolsystem.people.PersonService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class StudentService
{

	private final PersonService personService;
	private final StudentRepository studentRepository;

	public StudentService(final PersonService personService,
						  final StudentRepository studentRepository)
	{
		this.personService = personService;
		this.studentRepository = studentRepository;
	}

	private static Student createFromRequest(final Integer studentId,
											 final Integer personId,
											 final StudentRequest studentRequest)
	{
		return new Student(studentId,
						   personId,
						   studentRequest.getGroupNumber());
	}

	public Integer persist(final StudentRequest studentRequest)
	{
		final Integer personId = personService.persist(studentRequest.getPersonRequest());
		final Student student = createFromRequest(null, personId, studentRequest);
		return studentRepository.persist(student);
	}

	public void update(final Integer studentId,
					   final StudentRequest studentRequest)
	{
		final Student student = getById(studentId);
		personService.update(student.personId(), studentRequest.getPersonRequest());
		final Student updatedStudent = createFromRequest(student.id(), student.personId(), studentRequest);
		studentRepository.update(updatedStudent);
	}

	public Student getById(final Integer id)
	{
		final List<Student> people = studentRepository.get(List.of(id));
		if (people.isEmpty())
		{
			return null;
		}
		return people.get(0);
	}

	public List<Student> getByIds(final Collection<Integer> ids)
	{
		if (ids.isEmpty())
		{
			return List.of();
		}
		return studentRepository.get(ids);
	}

	public List<Student> getAll()
	{
		return studentRepository.getAll();
	}

	public List<Student> getByGroup(final Integer groupNumber)
	{
		return studentRepository.getByGroup(groupNumber);
	}

	public Student getByPersonId(final Integer personId) {
		return studentRepository.getByPersonId(personId);
	}
}
