package eu.petartoshev.simpleschoolsystem.courseperson;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class CoursePersonService
{

	private final CoursePersonRepository coursePersonRepository;
	private final CoursePersonValidationService coursePersonValidationService;

	public CoursePersonService(final CoursePersonRepository coursePersonRepository,
							   final CoursePersonValidationService coursePersonValidationService)
	{
		this.coursePersonRepository = coursePersonRepository;
		this.coursePersonValidationService = coursePersonValidationService;
	}

	public Integer persist(@RequestBody final CoursePersonRequest coursePersonRequest)
	{
		final CoursePerson coursePerson = createFromRequest(null, coursePersonRequest);
		return coursePersonRepository.persist(coursePerson);
	}

	public void delete(@PathVariable final Integer coursePersonId)
	{
		coursePersonRepository.delete(coursePersonId);
	}

	public List<CoursePerson> search(@RequestBody final CoursePersonSearchRequest searchRequest)
	{
		return coursePersonRepository.search(searchRequest);
	}

	public List<CoursePerson> getAll()
	{
		return coursePersonRepository.getAll();
	}

	private CoursePerson createFromRequest(final Integer coursePersonId,
										   final CoursePersonRequest coursePersonRequest)
	{
		coursePersonValidationService.validate(coursePersonRequest);
		return new CoursePerson(coursePersonId,
								coursePersonRequest.getCourseId(),
								coursePersonRequest.getPersonId(),
								coursePersonRequest.getType());
	}
}
