package eu.petartoshev.simpleschoolsystem.courseperson;

public class CoursePersonRequest
{
	private Integer courseId;
	private Integer personId;
	private CoursePersonType type;

	public CoursePersonRequest()
	{
	}

	public CoursePersonRequest(final Integer courseId,
							   final Integer personId,
							   final CoursePersonType type)
	{
		this.courseId = courseId;
		this.personId = personId;
		this.type = type;
	}

	public Integer getCourseId()
	{
		return courseId;
	}

	public Integer getPersonId()
	{
		return personId;
	}

	public CoursePersonType getType()
	{
		return type;
	}
}
