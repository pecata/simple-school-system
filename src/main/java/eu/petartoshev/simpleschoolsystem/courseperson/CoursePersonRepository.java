package eu.petartoshev.simpleschoolsystem.courseperson;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Repository
class CoursePersonRepository
		extends NamedParameterJdbcDaoSupport
{

	private static final CoursePersonRowMapper COURSE_ROW_MAPPER = new CoursePersonRowMapper();

	public CoursePersonRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	private static SqlParameterSource toParameters(final CoursePerson coursePerson)
	{
		return new MapSqlParameterSource()
				.addValue("id", coursePerson.id())
				.addValue("courseId", coursePerson.courseId())
				.addValue("personId", coursePerson.personId())
				.addValue("type", coursePerson.type().name(), Types.OTHER);
	}

	private static SqlParameterSource getSearchParameters(final CoursePersonSearchRequest searchRequest)
	{
		final MapSqlParameterSource params = new MapSqlParameterSource();
		if (hasValues(searchRequest.getCourseIds()))
		{
			params.addValue("courseIds", searchRequest.getCourseIds());
		}
		if (hasValues(searchRequest.getPeopleIds()))
		{
			params.addValue("personIds", searchRequest.getPeopleIds());
		}
		if (hasValues(searchRequest.getTypes()))
		{
			params.addValue("types", searchRequest.getTypes());
		}
		return params;
	}

	private static boolean hasValues(final List<?> list)
	{
		return list != null && !list.isEmpty();
	}

	private static String getSearchQuery(final CoursePersonSearchRequest searchRequest)
	{
		final String select = "SELECT * FROM courses_people";
		final List<String> whereClauses = new LinkedList<>();

		if (hasValues(searchRequest.getCourseIds()))
		{
			whereClauses.add("course_id IN (:courseIds)");
		}
		if (hasValues(searchRequest.getPeopleIds()))
		{
			whereClauses.add("person_id IN (:personIds)");
		}
		if (hasValues(searchRequest.getTypes()))
		{
			whereClauses.add("type IN (:types)");
		}

		if (whereClauses.isEmpty())
		{
			return select;
		}
		return select + " WHERE " + String.join(" AND ", whereClauses);
	}

	public Integer persist(final CoursePerson coursePerson)
	{
		final String sql = """
				INSERT INTO courses_people (course_id,
				                            person_id,
				                            type)
				 VALUES(:courseId,
				        :personId,
				        :type)
				""";
		final SqlParameterSource params = toParameters(coursePerson);

		final KeyHolder keyHolder = new GeneratedKeyHolder();
		getNamedParameterJdbcTemplate().update(sql, params, keyHolder, new String[]{"id"});
		return (Integer) keyHolder.getKey();
	}

	public void update(final CoursePerson coursePerson)
	{
		final String sql = """
				UPDATE courses_people
				   SET course_id = :courseId,
				       person_id = :personId,
				       type = :type
				 WHERE id = :id
				""";
		final SqlParameterSource params = toParameters(coursePerson);

		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public void delete(final Integer id)
	{
		final String sql = "DELETE FROM courses_people WHERE id = :id";
		final Map<String, Object> params = Map.of("id", id);
		getNamedParameterJdbcTemplate().update(sql, params);
	}

	public List<CoursePerson> get(final Collection<Integer> ids)
	{
		final String sql = "SELECT * FROM courses_people WHERE id IN (:ids)";
		final Map<String, Object> params = Map.of("ids", ids);
		return getNamedParameterJdbcTemplate().query(sql, params, COURSE_ROW_MAPPER);
	}

	public List<CoursePerson> search(final CoursePersonSearchRequest searchRequest)
	{
		final String sql = getSearchQuery(searchRequest);
		final SqlParameterSource params = getSearchParameters(searchRequest);
		return getNamedParameterJdbcTemplate().query(sql, params, COURSE_ROW_MAPPER);
	}

	public List<CoursePerson> getAll()
	{
		final String sql = "SELECT * FROM courses_people";
		return getNamedParameterJdbcTemplate().query(sql, COURSE_ROW_MAPPER);
	}

	private static class CoursePersonRowMapper
			implements RowMapper<CoursePerson>
	{

		@Override
		public CoursePerson mapRow(final ResultSet rs,
								   final int rowNum)
				throws SQLException
		{
			final Integer id = rs.getInt("id");
			final Integer courseId = rs.getInt("course_id");
			final Integer personId = rs.getInt("person_id");
			final CoursePersonType type = CoursePersonType.valueOf(rs.getString("type"));

			return new CoursePerson(id,
									courseId,
									personId,
									type);
		}
	}
}
