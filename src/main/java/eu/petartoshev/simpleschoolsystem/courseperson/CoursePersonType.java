package eu.petartoshev.simpleschoolsystem.courseperson;

public enum CoursePersonType
{
	STUDENT,
	TEACHER
}
