package eu.petartoshev.simpleschoolsystem.courseperson;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/course-person/")
public class CoursePersonController
{

	private final CoursePersonService coursePersonService;

	public CoursePersonController(final CoursePersonService coursePersonService)
	{
		this.coursePersonService = coursePersonService;
	}

	@GetMapping
	public List<CoursePerson> getAll() {
		return coursePersonService.getAll();
	}

	@PostMapping
	public void add(@RequestBody final CoursePersonRequest coursePersonRequest)
	{
		coursePersonService.persist(coursePersonRequest);
	}

	@DeleteMapping("/{coursePersonId}")
	public void delete(@PathVariable final Integer coursePersonId)
	{
		coursePersonService.delete(coursePersonId);
	}

	@PostMapping("/search")
	public List<CoursePerson> search(@RequestBody final CoursePersonSearchRequest searchRequest)
	{
		return coursePersonService.search(searchRequest);
	}
}
