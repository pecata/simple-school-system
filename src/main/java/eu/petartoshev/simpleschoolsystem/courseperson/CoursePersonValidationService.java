package eu.petartoshev.simpleschoolsystem.courseperson;

import eu.petartoshev.simpleschoolsystem.course.CourseService;
import eu.petartoshev.simpleschoolsystem.student.Student;
import eu.petartoshev.simpleschoolsystem.student.StudentService;
import eu.petartoshev.simpleschoolsystem.teacher.Teacher;
import eu.petartoshev.simpleschoolsystem.teacher.TeacherService;
import org.springframework.stereotype.Service;

@Service
class CoursePersonValidationService
{

	private final CourseService courseService;
	private final StudentService studentService;
	private final TeacherService teacherService;

	CoursePersonValidationService(final CourseService courseService,
								  final StudentService studentService,
								  final TeacherService teacherService)
	{
		this.courseService = courseService;
		this.studentService = studentService;
		this.teacherService = teacherService;
	}

	void validate(final CoursePersonRequest request)
	{
		if (request.getCourseId() == null || courseService.getById(request.getCourseId()) == null)
		{
			throw new CoursePersonDataException("No such course");
		}

		if (request.getType() == null)
		{
			throw new CoursePersonDataException("No such course type");
		}

		if (request.getPersonId() == null)
		{
			throw new CoursePersonDataException("No such person");
		}

		switch (request.getType())
		{
			case STUDENT -> {
				final Student s = studentService.getByPersonId(request.getPersonId());
				if (s == null)
				{
					throw new CoursePersonDataException("Not registered student with this person id");
				}
			}
			case TEACHER -> {
				final Teacher t = teacherService.getByPersonId(request.getPersonId());
				if (t == null)
				{
					throw new CoursePersonDataException("Not registered teacher with this person id");
				}
			}
		}
	}
}
