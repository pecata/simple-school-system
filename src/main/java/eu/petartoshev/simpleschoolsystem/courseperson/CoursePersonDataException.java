package eu.petartoshev.simpleschoolsystem.courseperson;

public class CoursePersonDataException
		extends RuntimeException
{

	public CoursePersonDataException(final String message)
	{
		super(message);
	}
}
