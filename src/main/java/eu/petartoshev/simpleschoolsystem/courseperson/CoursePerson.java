package eu.petartoshev.simpleschoolsystem.courseperson;

public record CoursePerson(Integer id,
						   Integer courseId,
						   Integer personId,
						   CoursePersonType type)
{


}
