package eu.petartoshev.simpleschoolsystem.courseperson;

import java.util.List;

public class CoursePersonSearchRequest
{

	private List<Integer> courseIds;
	private List<Integer> peopleIds;
	private List<CoursePersonType> types;

	public CoursePersonSearchRequest()
	{
	}

	public CoursePersonSearchRequest(final List<Integer> courseIds,
									 final List<Integer> peopleIds,
									 final List<CoursePersonType> types)
	{
		this.courseIds = courseIds;
		this.peopleIds = peopleIds;
		this.types = types;
	}

	public List<Integer> getCourseIds()
	{
		return courseIds;
	}

	public List<Integer> getPeopleIds()
	{
		return peopleIds;
	}

	public List<CoursePersonType> getTypes()
	{
		return types;
	}
}
