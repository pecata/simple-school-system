package eu.petartoshev.simpleschoolsystem.search;

import eu.petartoshev.simpleschoolsystem.student.Student;
import eu.petartoshev.simpleschoolsystem.student.StudentService;
import eu.petartoshev.simpleschoolsystem.teacher.Teacher;
import eu.petartoshev.simpleschoolsystem.teacher.TeacherService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class SearchService
{

	private final SearchRepository searchRepository;
	private final StudentService studentService;
	private final TeacherService teacherService;

	public SearchService(final SearchRepository searchRepository,
						 final StudentService studentService,
						 final TeacherService teacherService)
	{
		this.searchRepository = searchRepository;
		this.studentService = studentService;
		this.teacherService = teacherService;
	}

	public List<Teacher> getTeachersBySpecificGroupAndCourse(final Integer courseId,
															 final Short groupId)
	{
		final List<Integer> teacherIds = searchRepository.getTeacherIdsBySpecificGroupAndCourse(courseId, groupId);
		return teacherService.getByIds(teacherIds);
	}

	public List<Student> getStudentsBySpecificGroupAndCourse(final Integer courseId,
															 final Short groupId)
	{
		final List<Integer> studentsIds = searchRepository.getStudentsBySpecificGroupAndCourse(courseId, groupId);
		return studentService.getByIds(studentsIds);
	}

	public List<Student> getStudentsBySpecificMinAgeAndCourse(final Integer courseId,
															  final Short minimalAge)
	{
		final LocalDate birthday = LocalDate.now().minusYears(minimalAge);
		final List<Integer> studentsIds = searchRepository.getStudentsBySpecificMinAgeAndCourse(courseId, birthday);
		return studentService.getByIds(studentsIds);
	}
}
