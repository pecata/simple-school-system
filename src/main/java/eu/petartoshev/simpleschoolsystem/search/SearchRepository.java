package eu.petartoshev.simpleschoolsystem.search;

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@Repository
class SearchRepository
		extends NamedParameterJdbcDaoSupport
{

	public SearchRepository(final DataSource dataSource)
	{
		setDataSource(dataSource);
	}

	public List<Integer> getTeacherIdsBySpecificGroupAndCourse(final Integer courseId,
															   final Short groupId)
	{
		final String sql = """
				SELECT t.id 
				  FROM teachers t
				  JOIN courses_people cp ON t.person_id = cp.person_id AND cp.courseId = :courseId AND type = 'TEACHER'
				 WHERE t.group_number = :groupNumber 
				""";

		final Map<String, Object> params = Map.of("courseId", courseId,
												  "groupNumber", groupId);

		return getNamedParameterJdbcTemplate().queryForList(sql, params, Integer.class);
	}

	public List<Integer> getStudentsBySpecificGroupAndCourse(final Integer courseId,
															 final Short groupId)
	{
		final String sql = """
				SELECT s.id 
				  FROM students s
				  JOIN courses_people cp ON s.person_id = cp.person_id AND cp.courseId = :courseId AND type = 'STUDENT'
				 WHERE s.group_number = :groupNumber 
				""";

		final Map<String, Object> params = Map.of("courseId", courseId,
												  "groupNumber", groupId);

		return getNamedParameterJdbcTemplate().queryForList(sql, params, Integer.class);
	}

	public List<Integer> getStudentsBySpecificMinAgeAndCourse(final Integer courseId,
															  final LocalDate birthdayBefore)
	{
		final String sql = """
				SELECT s.id 
				  FROM students s
				  JOIN courses_people cp ON s.person_id = cp.person_id AND cp.course_id = :courseId AND type = 'STUDENT'
				  JOIN people p ON s.person_id = p.id AND p.birthday <= :birthdayBefore
				""";

		final Map<String, Object> params = Map.of("courseId", courseId,
												  "birthdayBefore", birthdayBefore);

		return getNamedParameterJdbcTemplate().queryForList(sql, params, Integer.class);
	}
}
