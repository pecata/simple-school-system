package eu.petartoshev.simpleschoolsystem.search;

import eu.petartoshev.simpleschoolsystem.student.Student;
import eu.petartoshev.simpleschoolsystem.teacher.Teacher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController
{

	private final SearchService searchService;

	public SearchController(final SearchService searchService)
	{
		this.searchService = searchService;
	}

	@GetMapping("/teachers/groups/{groupId}/courses/{courseId}")
	public List<Teacher> getTeachersBySpecificGroupAndCourse(@PathVariable final Integer courseId,
															 @PathVariable final Short groupId)
	{
		return searchService.getTeachersBySpecificGroupAndCourse(courseId, groupId);
	}

	@GetMapping("/students/groups/{groupId}/courses/{courseId}")
	public List<Student> getStudentsBySpecificGroupAndCourse(@PathVariable final Integer courseId,
															 @PathVariable final Short groupId)
	{
		return searchService.getStudentsBySpecificGroupAndCourse(courseId, groupId);
	}


	@GetMapping("/students/minimal-age/{minimalAge}/courses/{courseId}")
	public List<Student> getStudentsBySpecificMinAgeAndCourse(@PathVariable final Integer courseId,
															  @PathVariable final Short minimalAge)
	{
		return searchService.getStudentsBySpecificMinAgeAndCourse(courseId, minimalAge);
	}
}
