package eu.petartoshev.simpleschoolsystem.people;

import eu.petartoshev.simpleschoolsystem.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class PersonServiceTest
		extends ApplicationTests
{

	@Autowired
	private PersonService personService;

	private static void testEqualPerson(final PersonRequest personRequest,
										final Person person)
	{
		assertEquals(person.firstName(), personRequest.getFirstName());
		assertEquals(person.middleName(), personRequest.getMiddleName());
		assertEquals(person.lastName(), personRequest.getLastName());
		assertEquals(person.taxId(), personRequest.getTaxId());
		assertEquals(person.birthday(), personRequest.getBirthday());
	}

	@Test
	void testPersist()
	{
		final PersonRequest personRequest = new PersonRequest(
				"Ivan",
				"Draganov",
				"Petrov",
				"1234567890",
				LocalDate.of(2021, 12, 21)
		);

		final Integer persistId = personService.persist(personRequest);
		assertNotNull(persistId);

		final Person person = personService.getById(persistId);
		testEqualPerson(personRequest, person);
	}

	@Test
	void testUpdate()
	{
		final PersonRequest personRequestPersist = new PersonRequest(
				"Drago",
				"Dimitrov",
				"Ivanov",
				"0123456789",
				LocalDate.of(2020, 11, 20)
		);

		final Integer persistId = personService.persist(personRequestPersist);
		assertNotNull(persistId);

		final Person personPersisted = personService.getById(persistId);
		testEqualPerson(personRequestPersist, personPersisted);

		final PersonRequest personRequestUpdate = new PersonRequest(
				"Dragov",
				"Dimitrovv",
				"Ivanovv",
				"0123456788",
				LocalDate.of(2019, 10, 19)
		);

		personService.update(persistId, personRequestUpdate);

		final Person personUpdated = personService.getById(persistId);
		testEqualPerson(personRequestUpdate, personUpdated);

		assertNotEquals(personPersisted, personUpdated);
	}
}