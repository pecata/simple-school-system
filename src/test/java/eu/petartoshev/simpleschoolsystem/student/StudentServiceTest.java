package eu.petartoshev.simpleschoolsystem.student;

import eu.petartoshev.simpleschoolsystem.ApplicationTests;
import eu.petartoshev.simpleschoolsystem.people.Person;
import eu.petartoshev.simpleschoolsystem.people.PersonRequest;
import eu.petartoshev.simpleschoolsystem.people.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class StudentServiceTest
		extends ApplicationTests
{

	@Autowired
	private PersonService personService;

	@Autowired
	private StudentService studentService;

	private static void testEqualStudent(final StudentRequest studentRequest,
										 final Person person,
										 final Student student)
	{
		assertEquals(studentRequest.getGroupNumber(), student.groupNumber());
		assertEquals(person.id(), student.personId());
		assertEquals(person.taxId(), studentRequest.getPersonRequest().getTaxId());
	}

	private static PersonRequest getPersonRequest(final int taxIdSuffix)
	{
		return new PersonRequest(
				"Student",
				"Draganov",
				"Petrov",
				"s23456789" + taxIdSuffix,
				LocalDate.of(2021, 12, 21)
		);
	}

	@Test
	void testPersist()
	{
		final PersonRequest personRequest = getPersonRequest(0);
		final short groupNumber = 5;
		final StudentRequest studentRequest = new StudentRequest(personRequest, groupNumber);

		final Integer studentId = studentService.persist(studentRequest);
		assertNotNull(studentId);

		final Person person = personService.getByTaxId(personRequest.getTaxId());
		assertNotNull(person);

		final Student student = studentService.getById(studentId);
		testEqualStudent(studentRequest, person, student);
	}

	@Test
	void testUpdate()
	{
		final PersonRequest personRequestPersist = getPersonRequest(1);
		final short groupNumberPersist = 6;
		final StudentRequest studentRequestPersist = new StudentRequest(personRequestPersist, groupNumberPersist);

		final Integer studentId = studentService.persist(studentRequestPersist);
		assertNotNull(studentId);

		final Person personPersist = personService.getByTaxId(personRequestPersist.getTaxId());
		assertNotNull(personPersist);

		final Student studentPersist = studentService.getById(studentId);
		testEqualStudent(studentRequestPersist, personPersist, studentPersist);


		final PersonRequest personRequestUpdate = getPersonRequest(2);
		final short groupNumberUpdate = 7;
		final StudentRequest studentRequestUpdate = new StudentRequest(personRequestUpdate, groupNumberUpdate);

		studentService.update(studentId, studentRequestUpdate);

		final Person personUpdate = personService.getByTaxId(personRequestUpdate.getTaxId());
		assertNotNull(personUpdate);

		final Student studentUpdate = studentService.getById(studentId);
		testEqualStudent(studentRequestUpdate, personUpdate, studentUpdate);

		assertNotEquals(studentPersist, studentUpdate);
	}
}