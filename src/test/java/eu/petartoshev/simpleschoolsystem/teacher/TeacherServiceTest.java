package eu.petartoshev.simpleschoolsystem.teacher;

import eu.petartoshev.simpleschoolsystem.ApplicationTests;
import eu.petartoshev.simpleschoolsystem.people.Person;
import eu.petartoshev.simpleschoolsystem.people.PersonRequest;
import eu.petartoshev.simpleschoolsystem.people.PersonService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TeacherServiceTest
		extends ApplicationTests
{

	@Autowired
	private PersonService personService;

	@Autowired
	private TeacherService teacherService;

	private static void testEqualTeacher(final TeacherRequest teacherRequest,
										 final Person person,
										 final Teacher teacher)
	{
		assertEquals(teacherRequest.getGroupNumber(), teacher.groupNumber());
		assertEquals(person.id(), teacher.personId());
		assertEquals(person.taxId(), teacherRequest.getPersonRequest().getTaxId());
	}

	private static PersonRequest getPersonRequest(final int taxIdSuffix)
	{
		return new PersonRequest(
				"Ivan",
				"Draganov",
				"Petrov",
				"t23456789" + taxIdSuffix,
				LocalDate.of(2021, 12, 21)
		);
	}

	@Test
	void testPersist()
	{
		final PersonRequest personRequest = getPersonRequest(0);
		final short groupNumber = 5;
		final TeacherRequest teacherRequest = new TeacherRequest(personRequest, groupNumber);

		final Integer teacherId = teacherService.persist(teacherRequest);
		assertNotNull(teacherId);

		final Person person = personService.getByTaxId(personRequest.getTaxId());
		assertNotNull(person);

		final Teacher teacher = teacherService.getById(teacherId);
		testEqualTeacher(teacherRequest, person, teacher);
	}

	@Test
	void testUpdate()
	{
		final PersonRequest personRequestPersist = getPersonRequest(1);
		final short groupNumberPersist = 6;
		final TeacherRequest teacherRequestPersist = new TeacherRequest(personRequestPersist, groupNumberPersist);

		final Integer teacherId = teacherService.persist(teacherRequestPersist);
		assertNotNull(teacherId);

		final Person personPersist = personService.getByTaxId(personRequestPersist.getTaxId());
		assertNotNull(personPersist);

		final Teacher teacherPersist = teacherService.getById(teacherId);
		testEqualTeacher(teacherRequestPersist, personPersist, teacherPersist);


		final PersonRequest personRequestUpdate = getPersonRequest(2);
		final short groupNumberUpdate = 7;
		final TeacherRequest teacherRequestUpdate = new TeacherRequest(personRequestUpdate, groupNumberUpdate);

		teacherService.update(teacherId, teacherRequestUpdate);

		final Person personUpdate = personService.getByTaxId(personRequestUpdate.getTaxId());
		assertNotNull(personUpdate);

		final Teacher teacherUpdate = teacherService.getById(teacherId);
		testEqualTeacher(teacherRequestUpdate, personUpdate, teacherUpdate);

		assertNotEquals(teacherPersist, teacherUpdate);
	}
}