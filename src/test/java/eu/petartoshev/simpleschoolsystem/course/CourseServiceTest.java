package eu.petartoshev.simpleschoolsystem.course;

import eu.petartoshev.simpleschoolsystem.ApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CourseServiceTest
		extends ApplicationTests
{

	@Autowired
	private CourseService courseService;


	private static void testEqualCourse(final CourseRequest courseRequest,
										final Course course)
	{
		assertEquals(course.name(), courseRequest.getName());
		assertEquals(course.type(), courseRequest.getType());
	}

	@Test
	void testPersist()
	{
		final CourseRequest courseRequest = new CourseRequest("gosho", CourseType.MAIN);
		final Integer courseId = courseService.persist(courseRequest);
		assertNotNull(courseId);

		final Course course = courseService.getById(courseId);
		assertNotNull(course);

		testEqualCourse(courseRequest, course);
	}

	@Test
	void testUpdate()
	{
		final CourseRequest courseRequestPersist = new CourseRequest("gosh2", CourseType.MAIN);
		final Integer courseId = courseService.persist(courseRequestPersist);
		assertNotNull(courseId);

		final Course coursePersist = courseService.getById(courseId);
		assertNotNull(coursePersist);

		final CourseRequest courseRequestUpdate = new CourseRequest("gosh3", CourseType.SECONDARY);
		courseService.update(courseId, courseRequestUpdate);

		final Course courseUpdate = courseService.getById(courseId);
		assertNotNull(courseUpdate);
		testEqualCourse(courseRequestUpdate, courseUpdate);

		assertNotEquals(coursePersist, courseUpdate);
	}

	@Test
	void testDuplicate()
	{
		final CourseRequest courseRequest = new CourseRequest("gosh1", CourseType.MAIN);
		courseService.persist(courseRequest);
		assertThrows(DuplicateKeyException.class, () -> courseService.persist(courseRequest));
	}
}